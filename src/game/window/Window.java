package game.window;

import game.util.ImageLoader;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

public class Window {

	
	
	public static void createWindow(int width, int height, String title) {

		//sets title
		Display.setTitle(title);

		
		try {

			//creates display with background colour
			Display.setDisplayMode(new DisplayMode(width, height));
			
			Display.create();

		} catch (LWJGLException e) {

			e.printStackTrace();
			System.exit(0);
		}

		glInit(width, height);
		
		

	}

	private static void glInit(int width, int height) {

		GL11.glEnable(GL11.GL_TEXTURE_2D);

		//base colour
		GL11.glClearColor(1.0f, 0.5f, 0.25f, 1.0f);

		// enable alpha blending
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		//sets OpenGL draw area 
		GL11.glViewport(0, 0, width, height);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, width, height, 0, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		
		
		
		
	}
	
	public static void glCleanUp() {

		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);

		
		
		
	}

	public static void update() {

		Display.update();

	}

	public static void destroy() {
		Display.destroy();
	}

	public static boolean isClosed() {
		return Display.isCloseRequested();
	}

}
