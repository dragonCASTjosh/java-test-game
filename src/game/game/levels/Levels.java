package game.game.levels;

import java.io.IOException;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.openal.Audio;

import game.Game;
import game.entity.Player;
import game.util.SoundLoader;
import game.util.Time;
import game.window.Window;

public class Levels {


	public static Player test;
	public static Audio testSong;

	public static void level1() {
		
			testSong.playAsSoundEffect(1.0f, 0.001f, false);
			test.update();
			
			renderLevel1();
		
	}

	private static void renderLevel1() {

		test.draw();
	}

	public static void initLevel1() throws IOException {
		
		test = new Player("player");
		test.init(Display.getWidth() /2 - test.getTexture().getImageWidth(), Display.getHeight()/2 - test.getTexture().getImageHeight(), 0.5F, 0);
		
		SoundLoader.loadSong("Machinimasound");
		testSong = SoundLoader.getSong("Machinimasound");
		
	}
	
	

}
