package game;

import java.io.IOException;

import game.core.GameObject;
import game.entity.Player;
import game.game.levels.Levels;
import game.util.ImageLoader;
import game.util.SoundLoader;
import game.util.Time;
import game.window.Window;

import org.lwjgl.input.Keyboard;
import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.openal.SoundStore;
import org.newdawn.slick.opengl.Texture;

public class Game {

	
	public static Texture loading;

	public static boolean fristRun = true;
	public static int level = 0;

	private static boolean levelSwitch;

	private long lastFPS;

	public Game() {

	}

	public void start() {

		try {
			ImageLoader.loadTexture("tempLoading");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loading = ImageLoader.getTexture("tempLoading");
		Time.getDelta();
		lastFPS = Time.getTime();
		levelSwitch = true;
		
		
		update();

	}
	

	public static void stop(int error) {
		cleanUp();

		if (error == 0) {
			System.exit(0);
		}
	}

	private static void update() {

		while (!Window.isClosed()) {

			if (fristRun) {
				
				reanderLoadingScrean();
				fristRun = false;
				
			} else {
				
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
				
				switch (level)
				{
				case 0 : 
					if(levelSwitch)
					{	
						try {
							Levels.initLevel1();
						} catch (IOException e) {
							
							e.printStackTrace();
						}
						levelSwitch = false;
					}
					Levels.level1();
					
					render();
				default: 
					break;
				}

				

				//render();

				SoundStore.get().poll(0);
			}
		}

		stop(1);

	}

	



	private static void reanderLoadingScrean() {

		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

		Color.white.bind();
		loading.bind(); // or GL11.glBind(texture.getTextureID());

		// creates a squire to be drawn
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0, 0);
		GL11.glVertex2f(0, 0);
		GL11.glTexCoord2f(1, 0);
		GL11.glVertex2f(0 + loading.getTextureWidth(), 0);
		GL11.glTexCoord2f(1, 1);
		GL11.glVertex2f(0 + loading.getTextureWidth(),
				0 + loading.getTextureHeight());
		GL11.glTexCoord2f(0, 1);
		GL11.glVertex2f(0, 0 + loading.getTextureHeight());
		GL11.glEnd();

		Display.update();
		
	}

	private static void render() {

		
		
		// updates the window
		Window.update();
	}

	private static void cleanUp() {
		
		AL.destroy();
		Window.glCleanUp();
		Window.destroy();
		System.exit(0);	
	}
}
