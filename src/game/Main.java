package game;

import game.window.Window;

public class Main {

	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	public static final String TITLE = "Game";
	
	public static void main(String[] args) {

		Window.createWindow(WIDTH, HEIGHT, TITLE);
		Game game = new Game();
		game.start();
		
	}

}
