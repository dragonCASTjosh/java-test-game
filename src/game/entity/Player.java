package game.entity;

import java.io.IOException;

import game.core.GameObject;
import game.util.mouse.GameMosue;
import net.java.games.input.Component.Identifier.Key;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Player extends GameObject {

	private float y;
	private float x;
	public boolean left = true;
	public boolean right = true;
	public boolean up = true;
	public boolean down = true;

	public Player(String textureName) throws IOException {
		super(textureName);

	}

	@Override
	public void update() {

		MovePlayer();
		setRotation(lookAtMouse());
	
	}

	private void MovePlayer() {

		if (!(getY() < 0) && up ) {
			if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
				if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
					this.y -= 0.5f;
				} else {
					this.y -= 0.3f;
				}
			}
		}
		if (!(y > 600) && down) {
			if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
				if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
					this.y += 0.5f;
				} else {
					this.y += 0.3f;
				}
			}
		}
		if (!(x < 0) && left) {

			if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
				if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
					this.x -= 0.5f;
				} else {
					this.x -= 0.3f;
				}
			}
		}
		if (!(x > 800) && right) {
			if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
				if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
					this.x += 0.5f;
				} else {
					this.x += 0.3f;
				}
			}
		}
		this.setX(x);
		this.setY(y);

	}

	private double lookAtMouse() {

		return Math.atan2(GameMosue.getMouseY() - this.y, GameMosue.getMouseX()
				- this.x);

	}

}
