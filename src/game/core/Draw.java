package game.core;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

public class Draw {

	/*
	 * Draws a textures square
	 * 
	 * @param x this is the x coord to draw from
	 * @param y this is the y coord to draw from
	 * @oaram texture this is the texture to draw
	 * @param scale scale of the image
	 * @param rotation the roation for the image
	 */
	public static void drawSquare(float x, float y, Texture texture, float scale, double rotation) {

		int rX = texture.getImageWidth() / 2;
		int rY = texture.getImageHeight() / 2;

		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, 0);
		GL11.glRotatef((float) Math.toDegrees(rotation), 0, 0, 1);
		GL11.glScalef(scale, scale, 0);
		
		// creates a square to be drawn
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getTextureID());
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0, 0);
		GL11.glVertex2f(-rX, -rY);
		GL11.glTexCoord2f(1, 0);
		GL11.glVertex2f(+rX, -rY);
		GL11.glTexCoord2f(1, 1);
		GL11.glVertex2f(+rX, +rY);
		GL11.glTexCoord2f(0, 1);
		GL11.glVertex2f(-rX, +rY);
		GL11.glEnd();

		GL11.glPopMatrix();

	}

}
