package game.core;

import java.io.IOException;

import game.util.ImageLoader;

import org.newdawn.slick.opengl.Texture;

public class GameObject {

	private float x;
	private float y;
	private String textureName;
	private Texture texture;
	private float scale;
	private double rotation;

	public GameObject(String textureName) throws IOException{
		this.textureName = textureName;
		ImageLoader.loadTexture(textureName);
		texture = ImageLoader.getTexture(textureName);

	}

	public void init(float x, float y, float scale,double rotation){

		this.x = x;
		this.y = y;
		this.scale = scale;
		this.rotation = rotation;

		
	}

	public void draw() {
		Draw.drawSquare(x, y, texture, scale, rotation);
	}

	public void update() {
		
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public String getTextureName() {
		return textureName;
	}

	public void setTextureName(String textureName) {
		this.textureName = textureName;
	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

}
