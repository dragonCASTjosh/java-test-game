package game.core;

import java.io.IOException;

public class Collidable 
{
	public int x;
	public int y;
	public int xScalse;
	public int yScalse;
	
	public Collidable(int x, int y, int xScale, int yScalse)
	{
		this.x = x;
		this.y = y;
		this.xScalse = xScale;
		this.yScalse = yScalse;
	}
	
	

}
