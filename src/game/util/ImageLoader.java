package game.util;

import game.Game;

import java.io.IOException;
import java.util.HashMap;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class ImageLoader {

	// game image pool
	private static HashMap<String, Texture> textureMap = new HashMap<String, Texture>();

	public ImageLoader() {

	}

	/*
	 * loads texture from file
	 * @param Name This is the name of the file
	 */
	public static void loadTexture(String Name) throws IOException {

		// temp texture to be loaded into the textureMap
		Texture texture = null;

		// load texture into game
		texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/images/" + Name + ".png"));

		//logging message
		System.out.println("Texture loaded: " + texture);
		System.out.println(">> Image width: " + texture.getImageWidth());
		System.out.println(">> Image height: " + texture.getImageHeight());
		System.out.println(">> Texture width: " + texture.getTextureWidth());
		System.out.println(">> Texture height: " + texture.getTextureHeight());
		System.out.println(">> Texture ID: " + texture.getTextureID());

		//call the add function
		addToTextureMap(Name, texture);
	}

	private static void addToTextureMap(String Name, Texture texture) {
		
		//add texture to texture map
		textureMap.put(Name, texture);
	}

	/*
	 * gets texture from the resource pool
	 * @param name This is the name of the texture
	 * @return The texture from the resource pool
	 */
	public static Texture getTexture(String name) {
		
		//Retrieve from texture map
		return textureMap.get(name);
	}

}
