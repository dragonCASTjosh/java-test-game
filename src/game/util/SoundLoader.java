package game.util;

import java.io.IOException;
import java.util.HashMap;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class SoundLoader {

	// game image pool
	private static HashMap<String, Audio> soundEffectMap = new HashMap<String, Audio>();
	private static HashMap<String, Audio> songMap = new HashMap<String, Audio>();


	public SoundLoader() {

	}

	/*loads sound effect from files
	 * @param Name This is the name of the sound effect file
	 * 
	 */
	public static void loadSoundEffect(String Name) throws IOException {

		// temp texture to be loaded into the textureMap
		Audio sound = null;

		// load texture into game
		sound = AudioLoader.getAudio("OGG",
				ResourceLoader.getResourceAsStream("res/sounds/" + Name + ".ogg"));
		// logging message
		System.out.println("Sound loaded: " + sound);
		

		// call the add function
		addToTSoundEffectMap(Name, sound);
	}

	private static void addToTSoundEffectMap(String Name, Audio sound) {

		// add texture to texture map
		soundEffectMap.put(Name, sound);
	}

	/* gets sfx from resource pool
	 * @param Name This is the name of the sound effect file
	 * @return Audio
	 */
	public static Audio getSoundEffect(String name) {

		// Retrieve from texture map
		return soundEffectMap.get(name);
	}
	
	
	
	
	
	/*loads song effect from files
	 * @param Name This is the name of the song file
	 * 
	 */
	public static void loadSong(String Name) throws IOException {

		// temp texture to be loaded into the textureMap
		Audio song = null;

		// load texture into game
		song = AudioLoader.getAudio("OGG",
				ResourceLoader.getResourceAsStream("res/sounds/" + Name + ".ogg"));
		// logging message
		System.out.println("Sound loaded: " + song);
		

		// call the add function
		addToSongMap(Name, song);
	}

	private static void addToSongMap(String Name, Audio song) {

		// add texture to texture map
		songMap.put(Name, song);
	}

	/* gets song from resource pool
	 * @param Name This is the name of the song file
	 * @return Audio
	 */
	public static Audio getSong(String name) {

		// Retrieve from texture map
		return songMap.get(name);
	}

}
