package game.util.mouse;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class GameMosue {
	
	public static int getMouseX()
	{
		return Mouse.getX();
	}
	
	public static int getMouseY()
	{
		return Display.getHeight() - Mouse.getY();
	}

}
